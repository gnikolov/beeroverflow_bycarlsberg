#  BeerOverflow
### by A18 Carlsberg team: Grigor Nikolov & Ivan Ivanov  

### Access the [Trello board here](https://trello.com/b/I4K4e6Pm/beeroverflow "Trello board").

*BeerOverflow* - a web application for managing users and the beers they drink.
The mission of *BeerOverflow* is to allow users to get an insight on beers from all around the world and choose the next
best beer to drink.
